package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"syscall"
	"unsafe"
)

var kernel32 = syscall.NewLazyDLL("kernel32.dll")
var procOpenProcess = kernel32.NewProc("OpenProcess")
var procReadProcessMemory = kernel32.NewProc("ReadProcessMemory")

var psapi = syscall.NewLazyDLL("psapi.dll")
var procGetProcessImageFileNameA = psapi.NewProc("GetProcessImageFileNameA")
var procEnumProcessModules = psapi.NewProc("EnumProcessModules")
var procGetModuleFileNameExA = psapi.NewProc("GetModuleFileNameExA")

func main() {
	fmt.Println("[ ] Starting Enum-ScanProcessMemory\n")

	pid, _ := strconv.Atoi(os.Args[1])
	fmt.Println("[-] PID          :", pid)

	handle, _, _ := procOpenProcess.Call(uintptr(0xFFFF), uintptr(1), uintptr(pid))
	fmt.Println("[-] Handle       :", handle)

	baseAddress := getBaseAddress(handle)
	fmt.Println("[-] Base Address :", baseAddress, "\n")

	fmt.Println("[-] Matches:")
	scanMemory(handle, baseAddress)
}

func getBaseAddress(handle uintptr) int64 {
	// GetProcessImageFileNameA
	var fileNameBuffer [200]byte
	var fileSize uint32 = 200

	ret, _, _ := procGetProcessImageFileNameA.Call(handle, uintptr(unsafe.Pointer(&fileNameBuffer)), uintptr(fileSize))

	if ret == 0 {
		panic("GetProcessImageFileNameA failed")
	}

	fileName := bufferToString(fileNameBuffer)[24:]

	// EnumProcessModules
	var needed int32

	moduleHandles := make([]uintptr, 1024)
	moduleHandleSize := unsafe.Sizeof(moduleHandles[0])

	ret, _, _ = procEnumProcessModules.Call(handle, uintptr(unsafe.Pointer(&moduleHandles[0])), moduleHandleSize*uintptr(len(moduleHandles)), uintptr(unsafe.Pointer(&needed)))

	if ret == 0 {
		panic("EnumProcessModules failed")
	}

	// GetModuleFileNameExA
	var baseAddress uintptr

	for _, moduleHandle := range moduleHandles {
		if moduleHandle == 0 {
			break
		}

		var moduleNameBuffer [200]byte
		var moduleSize uint32 = 200

		ret, _, _ = procGetModuleFileNameExA.Call(handle, uintptr(moduleHandle), uintptr(unsafe.Pointer(&moduleNameBuffer)), uintptr(moduleSize))

		if ret == 0 {
			panic("GetModuleFileNameExA failed")
		}

		moduleName := bufferToString(moduleNameBuffer)[3:]

		if moduleName == fileName {
			baseAddress = uintptr(moduleHandle)
			break
		}
	}

	return int64(baseAddress)
}

func scanMemory(handle uintptr, baseAddress int64) {
	var buffer [200]byte
	var length uint32

	readBlockSize := 200

	for {
		ret, _, _ := procReadProcessMemory.Call(handle, uintptr(baseAddress), uintptr(unsafe.Pointer(&buffer[0])), uintptr(readBlockSize), uintptr(unsafe.Pointer(&length)))

		if ret == 0 {
			readBlockSize--
		} else {
			var memory string

			for _, char := range buffer {
				memory += string(char)
			}

			regexPattern := regexp.MustCompile(`(http(s)?:\/\/)?(www\.)?(\w){1,}\.(com|org|net)((\/)(\w){1,}(\.(\w){1,3})?)?`)
			regexMatches := regexPattern.FindAllString(memory, -1)

			for _, match := range regexMatches {
				fmt.Println("    [-]", match)
			}

			if readBlockSize < 200 {
				break
			}

			baseAddress += int64(readBlockSize)
		}
	}
}

func bufferToString(buffer [200]byte) string {
	var str string

	for _, char := range buffer {
		if char == 0 {
			break
		}

		str += string(char)
	}

	return str
}
