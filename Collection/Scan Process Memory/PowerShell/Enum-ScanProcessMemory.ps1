param (
    [Parameter(Mandatory = $true)]
    [Int]
    $ProcessPID
)

Write-Host "[ ] Starting Enum-ScanProcessMemory`n"

$process = Get-Process -Id $processPID
$processHandle = $process.Handle
$processBaseAddress = $process.MainModule.BaseAddress

Write-Host "[-] PID          : $ProcessPID"
Write-Host "[-] Handle       : $processHandle"
Write-Host "[-] Base Address : $processBaseAddress`n"

Write-Host "[-] Matches:"

$MemberDefinition = @'
[DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
public static extern bool ReadProcessMemory(int hProcess, Int64 lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesRead);
'@

$Kernel32 = Add-Type -MemberDefinition $MemberDefinition -Name 'Kernel32' -Namespace 'newnamespace' -PassThru

$readBlockSize = 200

while ($true) {
    $buffer = New-Object byte[]($readBlockSize)
    $numberOfBytesRead = 0

    $ret = $Kernel32::ReadProcessMemory($processHandle, $processBaseAddress, $buffer, $buffer.Length, [ref]$numberOfBytesRead)

    if ($ret) {
        $data = [System.Text.Encoding]::ASCII.GetString($buffer)

        $regexPattern = [regex]'(http(s)?:\/\/)?(www\.)?(\w){1,}\.(com|org|net)((\/)(\w){1,}(\.(\w){1,3})?)?'
        $regexMatches = $regexPattern.Matches($data)
        
        $regexMatches | Foreach-Object {
            Write-Host "    [-] $_"
        }

        if ($readBlockSize -lt 200) {
            break
        }

        $processBaseAddress += [Int64]$readBlockSize
    }
    else {
        $readBlockSize--
    }
}