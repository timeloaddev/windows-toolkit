package main

import (
	"fmt"
	"time"

	"github.com/atotto/clipboard"
)

func main() {
	fmt.Println("[ ] Starting Enum-Clipboard\n")

	for {
		clipboardData, _ := clipboard.ReadAll()
		fmt.Println("[-] " + clipboardData)
		time.Sleep(10 * time.Second)
	}
}