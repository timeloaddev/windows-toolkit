$path = "C:\Users\username\Documents\"
$user = "username"

$acl = Get-Acl $path
$identityReference = $acl.Access | Select -ExpandProperty IdentityReference
$accessControlType = $acl.Access | Select -ExpandProperty AccessControlType

$isWritable = $false

for ($i = 0; $i -le $identityReference.Length; $i++) {
    if (($identityReference[$i] -eq ($env:ComputerName + "\" + $user)) -and ($accessControlType[$i] -eq "Allow")) {
        $isWritable = $true
    }
}

if ($isWritable) {
    Write-Host "[+] User '$user' can modify '$path'"
}
else {
    Write-Host "[x] User '$user' cannot modify '$path'"
}