package main

import (
	"fmt"

	"golang.org/x/sys/windows/registry"
)

func main() {
	fmt.Println("[ ] Starting Enum-InstalledSoftware")
	uninstallRegLocation := "SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall"
	uninstallKey, _      := registry.OpenKey(registry.LOCAL_MACHINE, uninstallRegLocation, registry.ENUMERATE_SUB_KEYS)	
	defer uninstallKey.Close()

	// 0 means "read all"
	subkeyNames, _ := uninstallKey.ReadSubKeyNames(0)

	for _, subkey := range subkeyNames {
		subkeyRegLocation := uninstallRegLocation + "\\" + subkey
		subkeyKey, _      := registry.OpenKey(registry.LOCAL_MACHINE, subkeyRegLocation, registry.QUERY_VALUE)
		defer subkeyKey.Close()

		softwareName, _, _      := subkeyKey.GetStringValue("DisplayName")
		softwareVersion, _, _   := subkeyKey.GetStringValue("DisplayVersion")
		softwarePublisher, _, _ := subkeyKey.GetStringValue("Publisher")

		fmt.Println("")
		fmt.Println("[-] Name      : " + softwareName)
		fmt.Println("[-] Version   : " + softwareVersion)
		fmt.Println("[-] Publisher : " + softwarePublisher)
	}
}
