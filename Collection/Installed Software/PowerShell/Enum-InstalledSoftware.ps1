Write-Host "[ ] Starting Enum-InstalledSoftware"

$installedSoftware = Get-ItemProperty -Path "HKLM:SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*"

$installedSoftware | Foreach-Object {
    Write-Host "`n"
    Write-Host "[-] Name      : $($_ | Select-Object -ExpandProperty DisplayName)"
    Write-Host "[-] Version   : $($_ | Select-Object -ExpandProperty DisplayVersion)"
    Write-Host "[-] Publisher : $($_ | Select-Object -ExpandProperty Publisher)"
}
