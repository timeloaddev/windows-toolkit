Write-Host "[ ] Starting Enum-LocalUsers"

$ErrorAction = "SilentlyContinue"

$machineName = (Get-WMIObject win32_operatingsystem).CSName

$adsi = [ADSI]"WinNT://$machineName"

$adsi.Children | where {$_.SchemaClassName -eq 'user'} | Foreach-Object {
    Write-Host "`n"
    Write-Host "Name                      : $($_ | Select -ExpandProperty Name)"
    Write-Host "Full Name                 : $($_ | Select -ExpandProperty FullName)"
    Write-Host "Authentication Type       : $($_ | Select -ExpandProperty AuthenticationType)"
    Write-Host "Minimum Password Length   : $($_ | Select -ExpandProperty MinPasswordLength)"
    Write-Host "Bad Password Attempts     : $($_ | Select -ExpandProperty BadPasswordAttempts)"
    Write-Host "Max Bad Password Attempts : $($_ | Select -ExpandProperty MaxBadPasswordsAllowed)"
}
