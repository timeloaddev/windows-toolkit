Write-Host "[ ] Starting Enum-RunningProcesses`n"

(Get-Process | Select Id,ProcessName) | Foreach-Object {
    $processPID = $_ | Select -ExpandProperty Id
    $processName = $_ | Select -ExpandProperty ProcessName
    
    Write-Host "[-] $processPID$(" " * (6 - $processPID.ToString().Length))$processName"
}