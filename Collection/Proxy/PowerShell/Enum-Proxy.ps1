Write-Host "[ ] Starting Enum-Proxy`n"

$proxyConfiguration = [System.Net.WebProxy]::GetDefaultProxy()

Write-Host "[-] Address               : $($proxyConfiguration | Select -ExpandProperty Address)"
Write-Host "[-] BypassProxyOnLocal    : $($proxyConfiguration | Select -ExpandProperty BypassProxyOnLocal)"
Write-Host "[-] BypassList            : $($proxyConfiguration | Select -ExpandProperty BypassList)"
Write-Host "[-] Credentials           : $($proxyConfiguration | Select -ExpandProperty Credentials)"
Write-Host "[-] UseDefaultCredentials : $($proxyConfiguration | Select -ExpandProperty UseDefaultCredentials)"
Write-Host "[-] BypassArrayList       : $($proxyConfiguration | Select -ExpandProperty BypassArrayList)"
