$ErrorActionPreference = "SilentlyContinue"

Write-Host "[ ] Starting Enum-RecentRunCommands`n"

$registryKey = Get-ItemProperty -Path "HKCU:SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\RunMRU"

$registryKey.PSObject.Properties | Foreach-Object {
    if ($_.Value -like "*\1") {
        Write-Host "[-] $($_.Value.Split("\")[0])"
    }
}
