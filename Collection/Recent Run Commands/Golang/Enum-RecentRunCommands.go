package main

import (
	"fmt"
	"strings"

	"golang.org/x/sys/windows/registry"
)

func main() {
	fmt.Println("[ ] Starting Enum-RecentRunCommands\n")

	recentRunCommandsRegLocation := "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\RunMRU"
	recentRunCommandsKey, _ := registry.OpenKey(registry.CURRENT_USER, recentRunCommandsRegLocation, registry.ALL_ACCESS)
	defer recentRunCommandsKey.Close()

	// 0 means "read all"
	recentRunCommands, _ := recentRunCommandsKey.ReadValueNames(0)

	for _, runCommand := range recentRunCommands {
		value, _, _ := recentRunCommandsKey.GetStringValue(runCommand)

		if strings.HasSuffix(value, "\\1") {
			fmt.Println("[-] " + strings.Split(value, "\\")[0])
		}
	}
}