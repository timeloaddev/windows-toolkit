Write-Host "[ ] Starting Enum-Machine"

$osHandler = Get-WMIObject win32_operatingsystem

$hostname = $osHandler.CSName
$osName = $osHandler.Name.Split("|")[0]
$osVersion = $osHandler.Version
$architecture = $osHandler.OSArchitecture

Write-Host "[-] Hostname         : $hostname"
Write-Host "[-] Operating System : $osName"
Write-Host "[-] OS Version       : $osVersion"
Write-Host "[-] Architecture     : $architecture"
