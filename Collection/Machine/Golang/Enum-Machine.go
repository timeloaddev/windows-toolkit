package main

import (
    "fmt"
    "os"
    "runtime"
    "golang.org/x/sys/windows/registry"
)

func main() {
    fmt.Println("[ ] Starting Enum-Machine\n")
    
    hostname, _ := os.Hostname()
    fmt.Println("[-] Host Name        :" + hostname)
    
    key, _ := registry.OpenKey(registry.LOCAL_MACHINE, `SOFTWARE\Microsoft\Windows NT\CurrentVersion`, registry.QUERY_VALUE)
    
    osName, _, _ := key.GetStringValue("ProductName")
    fmt.Println("[-] Operating System : " + osName)
    
    osVersion, _, _ := key.GetStringValue("CurrentVersion")
    fmt.Println("[-] OS Version       : " + osVersion)
    
    osBuild, _, _ := key.GetStringValue("CurrentBuild")
    fmt.Println("[-] OS Build         : " + osBuild)
    
    architecture := runtime.GOARCH
    fmt.Println("[-] Architevture     : " + architecture)
}
