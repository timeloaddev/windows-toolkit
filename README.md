# Windows Toolkit
## What is Windows Toolkit?
Windows Toolkit is a collection of malware snippets targeted for Windows.
## Why build the Windows Toolkit?
I believe too many people rely on automated tools like Metasploit and don't have a deep enough understanding on how to write their own malware. This toolkit aims to provide open-source malware samples for those who are interested in developing their skills as a pentester or red teamer.
## How can I utilize the code provided in this toolkit?
Individually, the malware snippest won't do much. Windows Toolkit isn't designed to provide source code for complete reverse shells that are ready to be used in engagements. Instead, Windows Toolkit breaks down all of the elements of malware into individual techniques. It is your responsibility to learn how they work and implement them in your own code.