$ErrorActionPreference = "SilentlyContinue"

Write-Host "[ ] Starting Enum-Putty`n"

Write-Host "[ ] Saved Putty Sessions`n"

$puttySavedSessionNames = @()

Get-ChildItem -Path "HKCU:SOFTWARE\SimonTatham\PuTTY\Sessions" | Foreach-Object {
    $puttySavedSessionNames += ($_ | Select -ExpandProperty Name).Split("\")[5]
}

$puttySavedSessionNames | Foreach-Object {
    $registryKey = Get-ItemProperty -Path "HKCU:SOFTWARE\SimonTatham\PuTTY\Sessions\$_"

    Write-Host "[-] Name          : $_"
    Write-Host "[-] HostName      : $($registryKey.HostName)"
    Write-Host "[-] Protocol      : $($registryKey.Protocol)"
    Write-Host "[-] PortNumber    : $($registryKey.PortNumber)"
    Write-Host "[-] Username      : $($registryKey.Username)"
    Write-Host "[-] ProxyHost     : $(registryKey.ProxyHost)"
    Write-Host "[-] ProxyUsername : $(registryKey.ProxyUseranem)"
    Write-Host "[-] ProxyPassword : $(registryKey.ProxyPassword)"
    Write-Host "[-] PublicKeyFile : $(registryKey.PublicKeyFile)"
    Write-Host "`n"
}

Write-Host "[ ] Recent Putty Sessions `n"

$puttyRecentSessionNames = @()

Get-ChildItem -Path "HKCU:SOFTWARE\SimonTatham\PuTTY\Jumplist" | Foreach-Object {
    $puttyRecentSessionNames += ($_ | Select -ExpandProperty Name).Split("\")[5]
}

$puttyRecentSessionNames | Foreach-Object {
    $_
    Write-Host "`n"
}

Write-Host "[ ] Putty SSH Host Keys`n"

(Get-ItemProperty -Path "HKCU:SOFTWARE\SimonTatham\PuTTY\SshHostKeys").PSObject.Properties | Foreach-Object {
    if ($_.Name -like "*@*") {
        Write-Host "[-] Name  : $($_.Name)"
        Write-Host "[-] Value : $($_.Value)"
        Write-Host "`n"
    }
}
