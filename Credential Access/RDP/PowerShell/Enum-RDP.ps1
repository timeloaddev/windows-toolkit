$ErrorActionPreference = "SilentlyContinue"

Write-Host "[ ] Starting Enum-RDP`n"

Get-ItemProperty "HKCU:SOFTWARE\Microsoft\Terminal Server Client\Default" | Foreach-Object {
    Write-Host "[-] $_"
}
