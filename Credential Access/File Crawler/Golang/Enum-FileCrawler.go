package main

import (
	"fmt"
	"os"
	"strings"
	"bufio"
	"path/filepath"
)

var keywords [6]string

func main() {
	fmt.Println("[ ] Starting Enum-FileCrawler\n")
	
	keywords = [6]string { "user", "pass", "pwd", "login", "logon", "credential" }

	_ = filepath.Walk("C:\\Users\\Fletcher\\Documents\\", func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		} else if info.IsDir() && strings.HasPrefix(".", info.Name()) {
			return filepath.SkipDir
		} else if checkFilename(info.Name()) {
			fmt.Println("[-] Keyword in file name: " + path)
		} else if checkPlaintextContents(info.Name(), path) {
			fmt.Println("[-] Keyword in contents: " + path)
		}

		return nil
	})
}

func checkFilename(filename string) bool {
	for _, keyword := range keywords {
		if strings.Contains(filename, keyword) {
			return true
		}
	}

	return false
}

func checkPlaintextContents(filename string, path string) bool {
	formats := [8]string { ".txt", ".py", ".hta", ".vbs", ".js", ".bat", ".ps1", ".ps2" }

	for _, format := range formats {
		if strings.HasSuffix(filename, format) {
			file, _ := os.Open(path)
			defer file.Close()

			stats, _ := file.Stat()

			var size int64 = stats.Size()
			bytes := make([]byte, size)

			buffer := bufio.NewReader(file)
			_, _ = buffer.Read(bytes)

			contents := string(bytes)

			for _, keyword := range keywords {
				if strings.Contains(contents, keyword) {
					return true
				}
			}
		}
	}

	return false
}