package main

import (
    "fmt"
    "golang.org/x/sys/windows/registry"
)

func main() {
    fmt.Println("[ ] Starting Enum-AutoLogon")
    
    key, _ := registry.OpenKey(registry.LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon", registry.QUERY_VALUE)
    
    autoAdminLogon, _, _ := key.GetIntegerValue("AdminAutoLogon")
    defaultDomainName, _, _ := key.GetIntegerValue("DefaultDomainName")
    defaultUserName, _, _ := key.GetIntegerValue("DefaultUserName")
    defaultPassword, _, _ := key.GetIntegerValue("DefaultPassword")
    altDefaultDomainName, _, _ := key.GetIntegerValue("AltDefaultDomainName")
    altDefaultUserName, _, _ := key.GetIntegerValue("AltDefaultUserName")
    altDefaultPassword, _, _ := key.GetIntegerValue("AltDefaultPassword")
    
    fmt.Println("[-] AutoAdminLogon Value       : " + string(autoAdminLogon))
    fmt.Println("[-] DefaultDomainName Value    : " + string(defaultDomainName))
    fmt.Println("[-] DefaultUserName Value      : " + string(defaultUserName))
    fmt.Println("[-] DefaultPassword Value      : " + string(defaultPassword))
    fmt.Println("[-] AltDefaultDomainName Value : " + string(altDefaultDomainName))
    fmt.Println("[-] AltDefaultUserName Value   : " + string(altDefaultUserName))
    fmt.Println("[-] AltDefaultPassword Value   : " + string(altDefaultPassword))
    
    if autoAdminLogon == 1 {
        fmt.Println("[+] AutoLogon enabled")
    } else {
        fmt.Println("[x] AutoLogon not enabled")
    }
}
