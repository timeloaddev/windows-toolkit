Write-Host "[ ] Starting Enum-AutoLogon"

$winlogonPath = "HKLM:SOFTWARE\Microsoft\Windows NT\CurrentVersion\winlogon"

$autoAdminLogon = (Get-ItemProperty -Path $winlogonPath -Name AutoAdminLogon -ErrorAction SilentlyContinue).AutoAdminLogon
$defaultDomainName = (Get-ItemProperty -Path $winlogonPath -Name DefaultDomainName -ErrorAction SilentlyContinue).DefaultDomainName
$defaultUserName = (Get-ItemProperty -Path $winlogonPath -Name DefaultUserName -ErrorAction SilentlyContinue).DefaultUserName
$defaultPassword = (Get-ItemProperty -Path $winlogonPath -Name DefaultPassword -ErrorAction SilentlyContinue).DefaultPassword
$altDefaultDomainName = (Get-ItemProperty -Path $winlogonPath -Name AltDefaultDomainNamme -ErrorAction SilentlyContinue).AltDefaultDomainName
$altDefaultUserName = (Get-ItemProperty -Path $winlogonPath -Name AltDefaultUserName -ErrorAction SilentlyContinue).AltDefaultUserName
$altDefaultPassword = (Get-ItemProperty -Path $winlogonPath -Name AltDefaultPassword -ErrorAction SilentlyContinue).AltDefaultPassword

Write-Host "[-] AutoAdminLogon Value        : $autoAdminLogon"
Write-Host "[-] DefaultDomainNamee Value    : $defaultDomainName"
Write-Host "[-] DefaultUserName Value       : $defaultUserName"
Write-Host "[-] DefaultPassword Value       : $defaultPassword"
Write-Host "[-] AlltDefaultDomainName Value : $altDefaultDomainName"
Write-Host "[-] AltDefaultUserName Value    : $altDefaultUserName"
Write-Host "[-] AltDefaultPassword Value    : $altDefaultPassword"

if ($autoAdminLogon -eq 1) {
    Write-Host "[+] AutoLogon enabled"
}
else {
    Write-Host "[x] AutoLogon not enabled"
}
