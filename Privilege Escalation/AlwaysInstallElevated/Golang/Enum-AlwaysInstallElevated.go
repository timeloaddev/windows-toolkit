package main

import (
    "fmt"
    "golang.org/x/sys/windows/registry"
)

func main() {
    fmt.Println("[ ] Starting Enum-AlwaysInstallElevated")
    
    hklmKey, _ := registry.OpenKey(registry.LOCAL_MACHINE, "SOFTWARE\\Policies\\Microsoft\\Windows\\Installer", registry.QUERY_VALUE)
    defer hklmKey.Close()
    
    hkcuKey, _ := registry.OpenKey(registry.CURRENT_USER, "SOFTWARE\\Policies\\Microsoft\\Windows\\Installer", registry.QUERY_VALUE)
    defer hkcuKey.Close()
    
    hklmValue, _, _ := hklmKey.GetIntegerValue("AlwaysInstallElevated")
    hkcuValue, _, _ := hkcuKey.GetIntegerValue("AlwaysInstallElevated")
    
    fmt.Println("[-] HKLM Value : " + string(hklmValue))
    fmt.Println("[-] HKCU Value : " + string(hkcuValue))
    
    if (hklmValue == 1) && (hkcuValue == 1) {
        fmt.Println("[+] AlwaysInstallElevated enabled")
    } else {
        fmt.Println("[x] AlwaysInstallElevated not enabled")
    }
}
