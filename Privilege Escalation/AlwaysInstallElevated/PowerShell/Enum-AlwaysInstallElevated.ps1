$ErrorActionPreference = "SilentlyContinue"

Write-Host "[ ] Starting Enum-AlwaysInstallElevated"

hklmValue = (Get-ItemProperty -Path "HKLM:SOFTWARE\Policies\Microsoft\Windows\Installer" -Name AlwaysInstallElevated -ErrorAction SilentlyContinue).AlwaysInstallElevated

hkcuValue = (Get-ItemProperty -Path "HKCU:SOFTWARE\Policies\Microsoft\Windows\Installer" -Name AlwaysInstallElevated -ErrorAction SilentlyContinue).AlwaysInstallElevated

Write-Host "[-] HKLM Value : $hklmValue"
Write-Host "[-] HKCU Value : $hkcuValue"

if (($hklmValue -eq 1) -and ($hkcuValue -eq 1)) {
    Write-Host "[+] AlwaysInstallElevated enabled"
}
else {
    Write-Host "[x] AlwaysInstallElevated not enabled"
}
